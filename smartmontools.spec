Name:      smartmontools
Version:   7.3
Release:   1
Epoch:     1
Summary:   S.M.A.R.T. utility toolset
License:   GPLv2+
URL:       http://smartmontools.sourceforge.net/

Source0:   http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
Source1:   smartmontools.sysconf

BuildRequires:    groff gcc-c++ automake
BuildRequires:    libselinux-devel libcap-ng-devel systemd systemd-units systemd-devel

Requires(preun):  systemd-units
Requires(post):   systemd-units
Requires(postun): systemd-units


%description
smartmontools contains utilities that control and monitor storage
devices using the Self-Monitoring, Analysis and Reporting Technology
(SMART) system build into ATA/SATA and SCSI/SAS hard drives and
solid-state drives.  This is used to check the reliability of the
drive and to predict drive failures.



%package      help
Summary:      Including man files for smartmontools
Requires:     man

%description  help
This contains man files for the using of smartmontools.




%prep
%autosetup -n %{name}-%{version} -p2

%build
autoreconf -i
%configure --with-selinux --with-libcap-ng=yes --with-libsystemd --with-systemdsystemunitdir=%{_unitdir} --sysconfdir=%{_sysconfdir}/%{name}/ --with-systemdenvfile=%{_sysconfdir}/sysconfig/%{name}
make %{?_smp_mflags} CXXFLAGS="$RPM_OPT_FLAGS -fpie" LDFLAGS="-pie -Wl,-z,relro,-z,now"

sed -i 's|/etc/smartmontools/sysconfig|/etc/sysconfig|g' smartd.service

%install
%make_install

install -D -p -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/sysconfig/smartmontools
mkdir -p %{buildroot}%{_sysconfdir}/%{name}/smartd_warning.d
mkdir -p %{buildroot}%{_sharedstatedir}/%{name}

%preun
%systemd_preun smartd.service

%post
%systemd_post smartd.service

%postun
%systemd_postun_with_restart smartd.service

%files
%doc %{_docdir}/%{name}
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/smartd_warning.d
%config(noreplace) %{_sysconfdir}/%{name}/smartd.conf
%config(noreplace) %{_sysconfdir}/%{name}/smartd_warning.sh
%config(noreplace) %{_sysconfdir}/sysconfig/smartmontools
%{_unitdir}/smartd.service
%{_sbindir}/{smartd,update-smart-drivedb,smartctl}
%{_datadir}/%{name}
%{_sharedstatedir}/%{name}


%files help
%{_mandir}/man*/{smart*,update-smart*}


%changelog
* Mon Aug 08 2022 hkgy <kaguyahatu@outlook.com> - 1:7.3-1
- update package to 7.3

* Thu Jan 14 2021 yanglongkang <yanglongkang@huawei.com> - 7.2-1
- update package to 7.2

* Thu Jul 16 2020 yanglongkang <yanglongkang@huawei.com> - 7.1-1
- update package to 7.1

* Tue Jun 30 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 1:6.6-9
- renumber patches.

* Fri Jan 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:6.6-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add the "-q never" option while executing smartd command in smartd.service

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:6.6-7
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC:Repackage

* Wed Aug 21 2019 zhanghaibo <ted.zhang@huawei.com> - 1:6.6-6
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC:openEuler Debranding

* Wed Aug 21 2019 Su Weifeng <suweifeng1@huawei.com> - 1:6.6-5.h2
- Type:other
- ID:NA
- SUG:NA
- DESC:rename patches

* Thu Apr 18 2019 yangzhuangzhuang<yangzhuangzhuang1@huawei.com> - 1:6.6-5.h1
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:os_linux.cpp/os_netbsd.cpp: fix crash on --scan if WITH_NVME_DEVICESC is not set and '-d <type>' is not specified
       ataprint.cpp: Fix detection of Device Statistics log with 256 sectors (#922)
       drivedb.h: - Western Digital Red: WD80EZZX - USB: WD
       update-smart-drivedb.in: Include configured PATH in help and error messages
       os_linux.cpp: Add missing braces to 3ware SELinux code
       drivedb.h: fix Innolite Satadom D150QV entry (#939)
       os_freebsd.cpp: on error was setting set_nvme_err() to 1
       os_linux.cpp: Fix device scan crash on missing /proc/devices
       smartd.cpp: Always ignore failure of ATA SMART ENABLE command if
       os_linux.cpp: Fix '-d megaraid' open crash on missing /proc/devices
       os_darwin.cpp, os_freebsd.cpp: fix return value in error paths
       ataprint.cpp: Fix Form Factor string with bits set in reserved area
- Package init
